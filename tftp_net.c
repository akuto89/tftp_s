#include <tftp_net.h>
#include <sys/poll.h>
#include <stdio.h>
#include <errno.h>

int openudp (uint16_t port, struct in_addr *addr)
{
    int sockfd;
    struct sockaddr_in addr_local;
    sockfd = socket (AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        return -1;

    addr_local.sin_family       = AF_INET;
    addr_local.sin_port         = htons (port);
    addr_local.sin_addr.s_addr  = addr->s_addr;

    if (bind (sockfd, (struct sockaddr*)&addr_local, sizeof (struct sockaddr)))
        return -1;

    return sockfd;
}

void *myrecv (int sockfd, size_t *len_out,
        struct sockaddr_in *addr_rmt, int time_out)
{
    size_t msg_len;
    socklen_t addr_len = sizeof (struct sockaddr_in);
    struct pollfd cli;
    int ret;

    cli.fd      = sockfd;
    cli.events  = POLLIN;

    if (time_out != 0) {
        ret = poll (&cli, 1, time_out * 1000);
        if (ret == -1) {
            return NULL;
        }
        if (!ret) {
            return NULL;
        }
    }

    msg_len = recvfrom (sockfd, NULL, 0, MSG_TRUNC | MSG_PEEK,
            (struct sockaddr*)addr_rmt, &addr_len);
    if (msg_len == 0)
        return NULL;

    void *msg = malloc (msg_len);

    msg_len = recvfrom (sockfd, msg, msg_len, 0,
            (struct sockaddr*)addr_rmt, &addr_len);

    *len_out = msg_len;

    return msg;
}

void mysend (int sockfd, char *data, size_t len, struct sockaddr_in *addr_rmt)
{
    socklen_t addr_len = sizeof (struct sockaddr_in);
    sendto (sockfd, data, len, 0, 
            (struct sockaddr*)addr_rmt, addr_len);
}

void sendack (int sock, uint16_t block, struct sockaddr_in *dest)
{
    uint16_t ack[2];
    ack[0] = htons (ACK);
    ack[1] = htons (block);
    sendto (sock, ack, sizeof(ack), 0, (struct sockaddr*)dest, 
            sizeof (struct sockaddr_in));
}

void senderr (int sock, uint16_t ercode, struct sockaddr_in *dest)
{
    uint8_t err[5];
    *(uint16_t*)err = htons(ERR);
    *(uint16_t*)(err+2) = htons(ercode);
    err[4] = 0;
    sendto (sock, err, sizeof(err), 0, (struct sockaddr*)dest, 
            sizeof (struct sockaddr_in));
}

void derrno (int sock, struct sockaddr_in *dest)
{
    switch (errno) {
        case ENOENT:
            senderr (sock, ERR_NOTFOUND, dest);
            break;
        case EACCES:
        case EROFS:
            senderr (sock, ERR_ACCESSDENIER, dest);
            break;
        case EFBIG:
        case ENOSPC:
            senderr (sock, ERR_DISKFULL, dest);
            break;
        case EEXIST:
            senderr (sock, ERR_CLOBBER, dest);
            break;
        default:
            senderr (sock, ERR_UNKNOWN, dest);
    }
}
