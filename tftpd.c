#include <tftp_net.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#define DATA_LEN 512

struct in_addr addr;
char *open_w[20];

struct packet_data {
    uint16_t opcode;
    uint16_t block;
    char buf[DATA_LEN];
};

void sendfile (int sock_local, int fd,  struct sockaddr_in *addr_rmt)
{
    struct packet_data buf;
    int len = DATA_LEN;
    uint16_t block;
    int idx;

    buf.opcode = htons (DAT);

    for (block = 1; len == DATA_LEN; block++) {
        len = read (fd, buf.buf, DATA_LEN);
        buf.block = htons (block);
        for (idx = 0; idx < 6; idx++) {
            size_t len_ack;
            void *request;
            mysend(sock_local, (char*)&buf, len + 4, addr_rmt);
            request = myrecv (sock_local, &len_ack, addr_rmt, 10);
            if (request == NULL)
                continue;
            if (htons(*(uint16_t*)request) == ACK && 
                    htons(*(uint16_t*)(request+2)) == block) {
                free (request);
                break;
            }
            free (request);
        }
    }
}

void recvfile (int sock_local, int fd, struct sockaddr_in *addr_rmt)
{
    void *request;
    size_t msg_len;
    int idx;
    uint16_t block = 0;
    uint16_t opcode;

    do {
        for (idx = 0; idx < 6; idx++) {
            sendack (sock_local, block, addr_rmt);
            request = myrecv (sock_local, &msg_len, addr_rmt, 10);
            if (request != NULL)
                break;
        }

        if (request == NULL) break;
        opcode = htons(*(uint16_t*)request);
        if (opcode == ERR) {
            free (request);
            break;
        }
        if (opcode == DAT) {
            write(fd, request+4, msg_len - 4);
            block++;
        }
        free (request);
    } while(msg_len == 4 + DATA_LEN);
    sendack (sock_local, block, addr_rmt);
}

void *client (void *args)
{
    struct sockaddr_in *addr_remote;
    uint16_t opcode;
    char *filename;
    int sock_local;
    socklen_t addr_len; 
    int fd;
    int idx;

    addr_remote = (struct sockaddr_in *)args;
    addr_len = sizeof(struct sockaddr_in);
    opcode = *(uint16_t*)(args + addr_len);
    filename = (char*)(args + addr_len + 2);
    sock_local = openudp(0, &addr);

    if (sock_local < 0)
        return NULL;
    if (opcode == WRQ) {
        printf("WRQ\n");
        int b = -1;
        for (idx = 0; idx < 20; idx++) {
            if (open_w[idx] != NULL)
                if (!strcmp (filename, open_w[idx])) b = idx;
        }
        if (b == -1) {
            for (idx = 0; idx < 20; idx++)
                if (open_w[idx] == NULL) {
                    open_w[idx] = filename;
                    b = idx;
                    break;
                }
            if (b != -1) {
                if ((fd = open (filename, O_WRONLY)) != 1)
                        fd = creat (filename, 0666);
                if (fd < 0) {
                    derrno (sock_local, addr_remote);
                    open_w[idx] = NULL;
                    free (args);
                    return NULL;
                }
                recvfile (sock_local, fd, addr_remote);
                close(fd);
                open_w[idx] = NULL;
            }
        }
    }
    if (opcode == RRQ) {
        printf("RRQ\n");
        fd = open (filename, O_RDONLY);
        if (fd < 0) {
            derrno (sock_local, addr_remote);
            free (args);
            return NULL;
        }
        sendfile (sock_local, fd, addr_remote);
        close(fd);
    }

    free (args);
    return NULL;
}

int main (int argc, char *argv[])
{
    int sockfd;
    int idx;
    char path[255];

    struct sockaddr_in addr_remote;

    pthread_attr_t threadAttr;
    pthread_attr_init(&threadAttr); 
    pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED);

    if (argc <= 1) {
        printf ("Use --help for more information\n");
        return 0;
    } else if (!strcmp(argv[1], "--help")) {
        printf ("Use:\n");
        printf ("tftp-server [-ip [ip_interface]]\n");
        return 0;
    } else {
        for (idx = 1; idx < argc; ++idx) {
            if (!strcmp (argv[idx], "-ip")) {
                if (argc == idx + 1) {
                    printf ("Unknown parameter\n");
                    return 0;
                } else { 
                    if (inet_aton (argv[++idx], &addr) == 0) {
                        printf ("Error in ip\n");
                        return 0;
                    }
                }
            } else {
                printf ("Unknown parameter\n");
                return 0;
            }
        }
    }

    sockfd = openudp(69, &addr);

    if (sockfd < 0) {
        perror ("bind()\n");
        return -1;
    }
    for (idx = 0; idx < 20; idx++) {
        open_w[idx] = NULL;
    }
    while (1) {
        size_t      msg_len;
        uint16_t    opcode;

        void *request = myrecv(sockfd, &msg_len, &addr_remote, 0);
        opcode = htons(*(uint16_t*)request);
        if (opcode == RRQ || opcode == WRQ) {
            char *filename  = (char *)(request + 2);
            size_t name_len = strlen(filename);
            char *mode      = (char *)(filename  + name_len + 1);

            pthread_t pth;
            void *arg = malloc (sizeof (struct sockaddr_in) + 2 + name_len + 1);
            *(struct sockaddr_in*)arg = addr_remote;
            *(uint16_t *)(arg + sizeof(struct sockaddr_in)) = opcode;
            memcpy(arg + sizeof (struct sockaddr_in) + 2, filename, name_len + 1);
            pthread_create(&pth, &threadAttr, &client, arg);
        }

        if (request)
            free (request);
    }

    close(sockfd);
    sleep(10);
    return 0;
}
