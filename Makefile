CC=gcc
INCLUDE:=-I./
MAIN:=tftpd
LIB_NET:=libtftp_net.so
LIB:=-ltftp_net -lpthread
BINARY:=./tftp_s

all: $(BINARY)

$(BINARY): $(MAIN).o $(LIB_NET)
		$(CC) -g -O0 -o $(BINARY) $(MAIN).o -L. $(LIB) -Wl,-rpath,.

$(MAIN).o: $(MAIN).c
		$(CC) -c $(MAIN).c $(INCLUDE)

$(LIB_NET): tftp_net.o
		$(CC) -shared -o $(LIB_NET) tftp_net.o

tftp_net.o: tftp_net.c
		$(CC) -c -fPIC tftp_net.c $(INCLUDE)

clean:
		rm *.o *.so
		rm $(BINARY)
